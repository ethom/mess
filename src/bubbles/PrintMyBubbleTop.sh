#!/bin/sh
many_="_______________________________________________________________________________________________________________________________________________________________________________________________________________________________________"
my_message=$1
bb_len=$2
my_bubble=$(echo $my_message | sed 's/my_/bubble_/')


width=$(stty size | sed 's/.*\ //')
white_space=$(($width-$bb_len))

## Start with first line of bubble top

# Calculate necessary width of first line of bubble top
name="Me"
top_bub_len=$(($bb_len-${#name}-2))

# > instead of >> to reset /tmp/bubble_X
printf "%*s%.*s\033[32;1m%s\033[0m_ \n" $white_space " " $top_bub_len $many_ $name > $my_bubble
#bubble_top=$(printf "%.*s%s \n" $top_bub_len $many_ "Me_") 
#bubble_top=$(printf "%*s%s" $top_bub_len $many_ "Me_ ") 
#bubble_top=$(printf "%${top_bub_len}s" "Me") 

#./src/bubbles/BubbleRightAlign.sh "$bubble_top" > $my_bubble
#./src/bubbles/BubbleRightAlign.sh "$bubble_top\\n" > $my_bubble
#echo "" >> $my_bubble


## Append second line of bubble top
#	> not it's just "*" not ".*" since we're not specifying contents.

printf "%*s%*s\n" $white_space "/" $bb_len "\\" >> $my_bubble
##printf "%${white_space}s${line}\n"
#bubble_middle=$(printf '%s%*s' "/" $bb_len '\\')
#bubble_middle=$(printf '%s%*s' "/" $((bb_len+1)) '\\')
#./src/bubbles/BubbleRightAlign.sh "$bubble_middle" >> $my_bubble
#echo "" >> $my_bubble

# Concatenate message into bubble
cat $my_message >> $my_bubble
