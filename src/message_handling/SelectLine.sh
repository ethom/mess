#!/bin/sh
cursor=$1
message="$2"
line_size=$3
#{#message}
start=$cursor
end=$(($cursor+$line_size))
echo "$message " | cut -c $start-$end | sed --posix "s/\(.*\)\ .*\$/\1/"
