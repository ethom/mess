#!/bin/sh
set -e 
set -u

max () {
	if [ $1 -gt $2 ]
	then
		echo $1
	else
		echo $2
	fi
}

message="$1"
my_message="$2"
his_message="$3"
who="$4"

./src/bubbles/PrintYourBubbleTop.sh $his_message $who

max_line_size=$(($(./src/init/GetScreenWidth.sh)/2))

##It would be better if X_thread was decrypted, tail'd and concatenated onto the new message in a single line
#  - bonus: rencrypt on that line too.


message_length=${#message}

# Seperate wrapping of messages for different screen sizes
line_range=1

# Min longest_line is "_Me_"
longest_line=5
while [ $line_range -le $message_length ]
do
	line="$(./src/message_handling/SelectLine.sh $line_range "$message" $max_line_size)"
	line_range=$(($line_range + ${#line}))

	# Keeps maxmimum value w/ intger
	longest_line=$(max ${#line} $longest_line)

	./src/message_handling/PrintLineToMyThread.sh "$line" $my_message 
done 

# Now wrap Bob's
# !!! BUG: how to know Bob's (his) screen size?
line_range=1
while [ $line_range -lt $message_length ]
do
	# !!! BUG assuming Bob's screen size
	line=$(./src/message_handling/SelectLine.sh $line_range "$message" 49)
	line_range=$(($line_range + ${#line}))
	./src/message_handling/PrintLineToYourThread.sh "$line" $his_message 
done


## Bubble the message.
#if [ $message_length -lt $max_line_size ]
#then
#	bb_len=$message_length
#else
#	bb_len=$max_line_size
#fi

#./src/bubbles/PrintMyBubbleTop.sh $my_message $bb_len
#./src/bubbles/PrintMyBubbleBottom.sh $my_message $bb_len
./src/bubbles/PrintMyBubbleTop.sh $my_message $longest_line
./src/bubbles/PrintMyBubbleBottom.sh $my_message $longest_line

./src/bubbles/PrintYourBubbleBottom.sh $his_message
