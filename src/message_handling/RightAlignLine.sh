#!/bin/sh
line="$1"
line_len=${#line}
width=$(stty size | sed 's/.*\ //')
white_space=$(($width-$line_len))
printf "%${white_space}s${line}\n"
