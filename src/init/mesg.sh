#!/bin/sh
# Largely main script
clean_up_command=$1
clear


who="Bob"
my_thread=$(cat "./.threads/$who/.my_thread")
my_message="/tmp/my_$my_thread"

his_thread=$(cat "./.threads/$who/.his_thread")
his_message="/tmp/his_$his_thread"

# Where to rsync to
his_path=$(cat "./.threads/$who/.his_path")
my_path=$(cat "./.threads/$who/.my_path")

my_full_path="$my_path/$my_thread"
his_full_path="$my_path/$his_thread"

gpg -d --quiet --yes -o $my_thread "$my_full_path.gpg"
gpg -d --quiet --yes -o $his_thread "$his_full_path.gpg"

read -r message 
while [ "$message" != "q" ]
do
	./src/message_handling/WrapMessage.sh "$message" "$my_message" "$his_message" "$who"
	
	cat $my_message >> $my_thread
	cat $his_message >> $his_thread

	shred -u $my_message $his_message

	## Re-encrypt	
	#gpg --armour --quiet --yes -e -r "E. S. Thomas" -o ./tmp/my_message.gpg /tmp/my_message
	#cat $my_thread $my_message.gpg > interm
	#gpg --quiet -d --allow-multiple-messages interm 2> /dev/null | gpg --armour --quiet --yes -e -r "E. S. Thomas" -o $my_thread.gpg 2> /dev/null 
	#rm interm
	
	#gpg --armour --quiet --yes -e -r "E. S. Thomas" -o /tmp/his_message.gpg /tmp/his_message
	#cat $his_thread.gpg /tmp/his_message.gpg > interm
	#gpg --quiet -d --allow-multiple-messages interm 2> /dev/null | gpg --armour --quiet --yes -e -r "E. S. Thomas" -o $his_thread.gpg 2> /dev/null 
	#rm interm	


	# Non-repetitive encryption
	gpg --armour --quiet --yes -e -r "E. S. Thomas" -o "$my_thread.gpg" "$my_thread" 2> /dev/null 
	gpg --armour --quiet --yes -e -r "E. S. Thomas" -o "$his_thread.gpg" "$his_thread" 2> /dev/null 

	rsync .a_thread.gpg $his_path
	rsync .b_thread.gpg $his_path

	clear
	read -r message 
done

# Don't need a final message because each thread is re-encrypted after each message
#gpg --armour --quiet --yes -e -r "E. S. Thomas" -o "$my_thread.gpg" $my_thread
#gpg --armour --quiet --yes -e -r "E. S. Thomas" -o "$his_thread.gpg" $his_thread

shred -u $my_thread $his_thread 
mv .a_thread.gpg $my_path
mv .b_thread.gpg $my_path

clear
exec $clean_up_command
