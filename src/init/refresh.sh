#!/bin/sh

# These two flags are important because otherwise when they find THREAD.GPG missing,
# it just rolls and rolls.
# THIS IS A BUG

#echo $1 >> input_var_test.txt
who="Bob"
height=$(./src/init/DetermineScreenHeight.sh)

# Sorta-useless, saves catting out an additional 3 lines
message_box_height=3
messages_height=$(($height-$message_box_height))

my_thread_name=$(cat "./.threads/$who/.my_thread")
my_encrypted_thread="$my_thread_name.gpg"
cp "./.threads/$who/.a_thread.gpg" ./
cp "./.threads/$who/.b_thread.gpg" ./
#tail -F $my_thread
while true 
do
	hash=$(md5sum $my_encrypted_thread | sed 's/\ .*//')

	clear
	gpg --quiet -d $my_encrypted_thread 2> /dev/null | tail -n $messages_height
	while [ "$(md5sum $my_encrypted_thread | sed 's/\ .*//')" = "$hash" ]
	do
		sleep 0.1
	done
done
##./src/cleanup/kill_screen.sh

