Mess
==================================================
*A Belligerently Minimalist Secure (Mess)aging App*
![](images/ascii/mess_ascii_bold.png)

Introduction
------------

Mess is a end-to-end encrypted and peer-to-peer messaging app.
It consists of 120 lines of Posix shell script, 
which tie together several established command-line applications including 
GNU Screen, Rsync, and GnuPG. 
Admittedly, Mess is an educational personal project. 
It has been a master class in Posix shell scripting.

Features
--------

- Double encrypted messaging (packet and tunnel)
- Portable across Posix platforms.
- Text alignment for sender and receiver.
- Works on a variety of terminals and screen sizes.
- Text bubbles.

The following are short-comings of Mess:
 
- It is impossible to send your counterpart only the letter 'q', since that quits 
  the program. 

![](images/screenshots/mess_demo.png)
