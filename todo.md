General Bugs
============

Find place to put ./src/cleanup/kill_screen.sh
----------------------------------------------

Only needed if screen (or tmux??) is used. For no errors to appear while using screen
it needs to be at bottom of "./src/init/mesg.sh". This causes problems when using 
xterm.

Security Issues
===============

Message Wrapping in Plain Text
------------------------------

### Problem
- For bubble fitting, each message is written line by line so to 
  wrap the message into a minimum width.
- This is done in plain text in /tmp/
- Attacker could watch /tmp/my_.a_thread or /tmp/bubble_.a_thread and watch
  what half of the conversation is.

### Possible Solutions
- Mitigation: Firejail (linux) provides a way for apps to request a private /tmp/
- Solution: Predict the line width in ./src/init/mesg.sh, and pass that through.
		- Therefore message could just stay in variable in "mesg.sh"
		- **Still relies on writting into /tmp/ for the text wrapping**


Formatting Issues
=================

Partner's messages are not wrapped
----------------------------------

### Problem
- No way of knowing the wrapping on his machine.
	> ! you can infer it from the last message he sent from HIS machine!


### Possible Solutions
- (partial) mitigation: ??? wrapping his messages on my machine?


Partner's message exceeds bubble when my screen is wider
--------------------------------------------------------

### Possible Solutions
 - Twice wrap the message?
 - Sync messages. Not threads


Empty Message sends a message
-----------------------------

### Possible Solution
 - Two conditions in the while loop?
 - Look up flags in "read"

Names are hard coded in
-----------------------



Use "exec" to cleanly exit?
---------------------------
 - screen doesn't let you use "exec"
	> although you sorta can? Couldn't figure out a ".screenrc" that it would
	  work on.


Sometimes starts w/ half empty screen?
--------------------------------------
